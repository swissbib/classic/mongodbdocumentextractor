# mongoDbDocumentExtractor

some utility scripts to extract documents from a noSQL Database (Mongo) using various Predicates (simple regex filter criteria....)

Documentation : https://wiki.biozentrum.unibas.ch/pages/viewpage.action?pageId=279533514
